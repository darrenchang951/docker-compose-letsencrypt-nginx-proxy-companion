#!/bin/bash
# 1. Check if .env file exists
if [ -e .env ]; then
    source .env
else
    echo "Continuing with default options defined in ./docker-compose.yml"
fi

# 2. Download the latest version of nginx.tmpl
mkdir -p ${NGINX_FILES_PATH}
curl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl > ${NGINX_FILES_PATH}/nginx.tmpl

# 3. Create Docker network
docker network create $NETWORK $NETWORK_OPTIONS

# 4. Start web proxy
docker-compose up -d

exit 0
